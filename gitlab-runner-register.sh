#!/bin/bash

ARG_RUNNER_URL="https://gitlab.com/"

while [ $# -gt 0 ]; do
  case "$1" in
    --runner-url=*)
      ARG_RUNNER_URL="${1#*=}"
      ;;
    --runner-name=*)
      ARG_RUNNER_NAME="${1#*=}"
      ;;
    --runner-tags=*)
      ARG_RUNNER_TAGS="${1#*=}"
      ;;
    --runner-token=*)
      ARG_RUNNER_TOKEN="${1#*=}"
      ;;
    --runner-limit=*)
      ARG_RUNNER_LIMIT="${1#*=}"
      ;;
    --runner-cache|--runner-cache=*)
      ARG_RUNNER_CACHE="${1#*=}"
      if [[ "$ARG_RUNNER_CACHE" -eq "--runner-cache" ]]; then
        ARG_RUNNER_CACHE="true"
      fi
      ;;
    --runner-cache-server-address=*)
      ARG_RUNNER_CACHE_SERVER_ADDRESS="${1#*=}"
      ;;
    --runner-cache-access-key=*)
      ARG_RUNNER_CACHE_ACCESS_KEY="${1#*=}"
      ;;
    --runner-cache-secret-key=*)
      ARG_RUNNER_CACHE_SECRET_KEY="${1#*=}"
      ;;
    --runner-cache-bucket-name=*)
      ARG_RUNNER_CACHE_BUCKET_NAME="${1#*=}"
      ;;
    --help)
      echo "Usage: gitlab-runner-register [args]"
      echo ""
      echo "All arguments:"
      echo ""
      echo "  --runner-cache                         "
      echo "  --runner-url=value                     Default is https://gitlab.com/"
      echo "  --runner-name=value                    "
      echo "  --runner-tags=value                    "
      echo "  --runner-token=value                   "
      echo "  --runner-limit=value                   "
      echo "  --runner-cache-server-address=value    "
      echo "  --runner-cache-access-key=value        "
      echo "  --runner-cache-secret-key=value        "
      echo "  --runner-cache-bucket-name=value       "
      exit 0
      ;;
    *)
      echo "Error: Invalid argument $1"
      exit 1
  esac
  shift
done

CMD="gitlab-runner register --non-interactive --url \"$ARG_RUNNER_URL\" --registration-token \"$ARG_RUNNER_TOKEN\" --run-untagged --docker-privileged --executor \"docker\" --docker-image \"alpine:latest\" --locked \"false\" --access-level \"not_protected\" --description \"$ARG_RUNNER_NAME\" --tag-list \"$ARG_RUNNER_TAGS\""

if [[ ! -z "$ARG_RUNNER_LIMIT" ]]; then
    CMD="$CMD --limit \"$ARG_RUNNER_LIMIT\""
fi

if [[ "$ARG_RUNNER_CACHE" -eq "true" ]]; then
    if [[ -z "$ARG_RUNNER_CACHE_BUCKET_NAME" ]]; then
        ARG_RUNNER_CACHE_BUCKET_NAME="runner"
    fi

    if [[ -z "$ARG_RUNNER_CACHE_SERVER_ADDRESS" ]]; then
        PORT=9005
        ARG_RUNNER_CACHE_SERVER_ADDRESS="$(curl http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address):$PORT"
        mkdir -p $HOME/minio-data/$ARG_RUNNER_CACHE_BUCKET_NAME
        docker run -d --restart always -p $PORT:9000 -v $HOME/.minio:/root/.minio -v $HOME/minio-data:/export -e "MINIO_ROOT_USER=$ARG_RUNNER_CACHE_ACCESS_KEY" -e "MINIO_ROOT_PASSWORD=$ARG_RUNNER_CACHE_SECRET_KEY" --name minio minio/minio:latest server /export
        ufw allow $PORT/tcp
        ufw reload
    fi

    CMD="$CMD --cache-shared --cache-s3-insecure --cache-type \"s3\" --cache-s3-server-address \"$ARG_RUNNER_CACHE_SERVER_ADDRESS\" --cache-s3-access-key \"$ARG_RUNNER_CACHE_ACCESS_KEY\" --cache-s3-secret-key \"$ARG_RUNNER_CACHE_SECRET_KEY\" --cache-s3-bucket-name \"$ARG_RUNNER_CACHE_BUCKET_NAME\""
fi

eval "$CMD"
gitlab-runner restart
gitlab-runner verify