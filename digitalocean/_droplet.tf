resource "digitalocean_droplet" "master" {
  image      = "docker-20-04"
  name       = "Giltab-Runner"
  region     = "sfo3"
  size       = "s-1vcpu-1gb"
  monitoring = true
  user_data  = file("droplet-config.yml")
  ssh_keys   = var.DIGITALOCEAN_SSH_KEYS
}
