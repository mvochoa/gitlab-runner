# Gitlab Runner <!-- omit in toc -->

Configuration files to deploy gitlab runner

- [Environment variables](#environment-variables)
- [Deploy](#deploy)

## Environment variables

Before making the deployment it is necessary to declare the following environment variables.

#### `TF_VAR_DIGITALOCEAN_TOKEN` <!-- omit in toc -->

This variable specifies the token that will be used for create the DigitalOcean droplet.

#### `TF_VAR_DIGITALOCEAN_SSH_KEYS` <!-- omit in toc -->

This variable specifies the ssh keys that will be used for the droplet. Example `TF_VAR_DIGITALOCEAN_SSH_KEYS='["16:b1:f7:99:ec:17:da:f9:e0:e5:70:96:f6:30:71:7c"]'`

#### `RUNNER_URL` *(optional)* <!-- omit in toc -->

This variable specifies the URL of the runner. By default it is `https://gitlab.com/`

#### `RUNNER_NAME` <!-- omit in toc -->

This variable specifies the name of the runner.

#### `RUNNER_TAGS` *(optional)* <!-- omit in toc -->

This variable specifies the tags that will be used for the gitlab jobs. Example `RUNNER_TAGS="tag1,tag2"`

#### `RUNNER_TOKEN` <!-- omit in toc -->

This variable specifies the token of the runner.

#### `RUNNER_LIMIT` *(optional)* <!-- omit in toc -->

This variable specifies how many jobs can be handled concurrently by this token. By default it is means do not limit.

#### `RUNNER_CACHE` <!-- omit in toc -->

This variable specifies that will be used for the cache. Example `RUNNER_CACHE="true"`

#### `RUNNER_CACHE_SERVER_ADDRESS` *(optional)* <!-- omit in toc -->

This variable specifies the server address of the cache server. By default it is the droplet IP.

#### `RUNNER_CACHE_ACCESS_KEY` <!-- omit in toc -->

This variable specifies the access key of the cache server. Minimum must be 8 characters

#### `RUNNER_CACHE_SECRET_KEY` <!-- omit in toc -->

This variable specifies the secret key of the cache server. Minimum must be 8 characters

#### `RUNNER_CACHE_BUCKET_NAME` *(optional)* <!-- omit in toc -->

This variable specifies the bucket name of the cache server. By default it is `runner`.

## Deploy

To create the droplet with the gitlab runner configuration it is necessary to run the following commands:

```bash
$ cd digitalocean/
$ envsubst < droplet-config.default.yml > droplet-config.yml
$ terraform init
$ terraform plan
$ terraform apply
```