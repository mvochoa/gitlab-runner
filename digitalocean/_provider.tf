variable "DIGITALOCEAN_TOKEN" {}
variable "DIGITALOCEAN_SSH_KEYS" {
    type = list(string)
}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = var.DIGITALOCEAN_TOKEN
}